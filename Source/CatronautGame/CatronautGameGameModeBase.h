// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CatronautGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CATRONAUTGAME_API ACatronautGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
