// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "CatronautPlayer.generated.h"

UCLASS()
class CATRONAUTGAME_API ACatronautPlayer : public ACharacter, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

		// ...

private:
	FGenericTeamId TeamId;

	virtual FGenericTeamId GetGenericTeamId() const override;

	// ...
public:
	// Sets default values for this character's properties
	ACatronautPlayer();

	

protected:
	ACatronautPlayer(const FObjectInitializer& ObjectInitializer);
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Handles input for moving
	UFUNCTION()
	void MoveForward(float Value);

	
	UFUNCTION()
	void MoveRight(float Value);

	// Handles Jumping.
	UFUNCTION()
		void StartJump();

	UFUNCTION()
		void StopJump();

};
