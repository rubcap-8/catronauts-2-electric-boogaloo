// Fill out your copyright notice in the Description page of Project Settings.

#include "CatronautPlayer.h"
#include "Engine/Engine.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/PlayerController.h"
#include "Camera/CameraComponent.h"

// Sets default values
ACatronautPlayer::ACatronautPlayer(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	TeamId = FGenericTeamId(0);

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

//	// Create a first person camera component.
//	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
//	// Attach the camera component to our capsule component.
//	CameraComponent->SetupAttachment(GetCapsuleComponent());
//
//	CameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 300.0f + BaseEyeHeight));
//	// Allow the pawn to control camera rotation.
//	CameraComponent->bUsePawnControlRotation = true;
}

FGenericTeamId ACatronautPlayer::GetGenericTeamId() const
{
	return TeamId;
}

// Called when the game starts or when spawned
void ACatronautPlayer::BeginPlay()
{
	Super::BeginPlay();

	if (GEngine)
	{
		// Put up a debug message for five seconds. The -1 "Key" value (first argument) indicates that we will never need to update or refresh this message.
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("We are using FPSCharacter."));
	}
}

// Called every frame
void ACatronautPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACatronautPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Movement
	PlayerInputComponent->BindAxis("MoveForward", this, &ACatronautPlayer::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACatronautPlayer::MoveRight);

	//Camera
	PlayerInputComponent->BindAxis("MouseX", this, &ACatronautPlayer::AddControllerYawInput);
	PlayerInputComponent->BindAxis("MouseY", this, &ACatronautPlayer::AddControllerPitchInput);

	//Jumping
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACatronautPlayer::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACatronautPlayer::StopJump);
}

void ACatronautPlayer::MoveForward(float Value)
{
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void ACatronautPlayer::MoveRight(float Value)
{
	// Find out which way is "right" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

void ACatronautPlayer::StartJump()
{
	bPressedJump = true;
}

void ACatronautPlayer::StopJump()
{
	bPressedJump = false;
}

